#!/usr/bin/env python3

import argparse
import fitz

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--input", required=True, help="Path to the pdf")
ap.add_argument("-o", "--output", required=False, help="Name of output file")
ap.add_argument("-n", "--name", required=False, help="Comment author name (default: anonymous)")
args = vars(ap.parse_args())

if args["name"] is None:
    author = "anonymous"
else:
    author = args["name"]
if args["output"] is None:
    outfile = "output.pdf"
else:
    outfile = args["output"]

pdf = fitz.open(args["input"])
page_count = pdf.pageCount
print(f"Opening a {page_count}-page pdf document.")

current_page = 1
current_comment = 1
for page in pdf.pages():
    firstrun = True
    for annot in page.annots():
        if 'title' in annot.info.keys():
            if firstrun is True:
                print(f"\n[{current_page}] ", end="")
            annot.setInfo(title=author)
            print(".", end="")
            firstrun = False
            current_comment += 1
    current_page += 1

if current_comment > 1:
    pdf.save(outfile)
    print("\n")
    print(f"Changed {current_comment} comments.")
else:
    print("No annotations with author, document left unchanged.")
