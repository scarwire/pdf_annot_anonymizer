# PDF comment anonymizer

This is a small command-line script that removes author information from all annotations in a PDF file.

## Requirements

This script requires Python 3 and [PyMuPDF](https://github.com/pymupdf/PyMuPDF).

## Usage

```
$ python3 pdf_annot_anonymizer.py -i input.pdf -o output.pdf
```
